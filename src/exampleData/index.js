export const productsSample = [
  {
    _id: '621db8ba8d22d4cbcb9f1fc2',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Amazon Echo Dot 3rd Generation',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/alexa.jpg',
    brand: 'Amazon',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Meet Echo Dot - Our most popular smart speaker with a fabric design. It is our most compact smart speaker that fits perfectly into small space',
    rating: 4,
    numReviews: 12,
    price: 29.99,
    countInStock: 0,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fc1',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Logitech G-Series Gaming Mouse',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/mouse.jpg',
    brand: 'Logitech',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Get a better handle on your games with this Logitech LIGHTSYNC gaming mouse. The six programmable buttons allow customization for a smooth playing experience',
    rating: 3.5,
    numReviews: 10,
    price: 49.99,
    countInStock: 7,
    isHaveAR: true,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fc0',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Sony Playstation 4 Pro White Version',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/playstation.jpg',
    brand: 'Sony',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'The ultimate home entertainment center starts with PlayStation. Whether you are into gaming, HD movies, television, music',
    rating: 3.5,
    numReviews: 12,
    price: 399.99,
    countInStock: 11,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fbf',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Cannon EOS 80D DSLR Camera',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/camera.jpg',
    brand: 'Cannon',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Characterized by versatile imaging specs, the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design',
    rating: 0,
    numReviews: 0,
    price: 929.99,
    countInStock: 5,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fbe',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'iPhone 11 Pro 256GB Memory',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/phone.jpg',
    brand: 'Apple',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Introducing the iPhone 11 Pro. A transformative triple-camera system that adds tons of capability without complexity. An unprecedented leap in battery life',
    rating: 4,
    numReviews: 8,
    price: 599.99,
    countInStock: 7,
    isHaveAR: true,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fbd',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Airpods Wireless Bluetooth Headphones',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/airpods.jpg',
    brand: 'Apple',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Bluetooth technology lets you connect it with compatible devices wirelessly High-quality AAC audio offers immersive listening experience Built-in microphone allows you to take calls while working',
    rating: 4.5,
    numReviews: 12,
    price: 89.99,
    countInStock: 10,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.562Z',
    updatedAt: '2022-03-01T06:10:02.562Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fbc',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Amazon Echo Dot 3rd Generation',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/alexa.jpg',
    brand: 'Amazon',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Meet Echo Dot - Our most popular smart speaker with a fabric design. It is our most compact smart speaker that fits perfectly into small space',
    rating: 4,
    numReviews: 12,
    price: 29.99,
    countInStock: 0,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.561Z',
    updatedAt: '2022-03-01T06:10:02.561Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fbb',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Logitech G-Series Gaming Mouse',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/mouse.jpg',
    brand: 'Logitech',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Get a better handle on your games with this Logitech LIGHTSYNC gaming mouse. The six programmable buttons allow customization for a smooth playing experience',
    rating: 3.5,
    numReviews: 10,
    price: 49.99,
    countInStock: 7,
    isHaveAR: true,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.561Z',
    updatedAt: '2022-03-01T06:10:02.561Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fba',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Sony Playstation 4 Pro White Version',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/playstation.jpg',
    brand: 'Sony',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'The ultimate home entertainment center starts with PlayStation. Whether you are into gaming, HD movies, television, music',
    rating: 3.5,
    numReviews: 12,
    price: 399.99,
    countInStock: 11,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.561Z',
    updatedAt: '2022-03-01T06:10:02.561Z'
  },
  {
    _id: '621db8ba8d22d4cbcb9f1fb9',
    user: '621db8ba8d22d4cbcb9f1f5e',
    name: 'Cannon EOS 80D DSLR Camera',
    image:
      'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/camera.jpg',
    brand: 'Cannon',
    category: {
      _id: '621db8ba8d22d4cbcb9f1f66',
      name: 'electronic',
      avatar:
        'https://arshop-bucket.s3.ap-southeast-1.amazonaws.com/images/electronic-category-avatar.jpg',
      __v: 0,
      createdAt: '2022-03-01T06:10:02.428Z',
      updatedAt: '2022-03-01T06:10:02.428Z'
    },
    description:
      'Characterized by versatile imaging specs, the Canon EOS 80D further clarifies itself using a pair of robust focusing systems and an intuitive design',
    rating: 0,
    numReviews: 0,
    price: 929.99,
    countInStock: 5,
    isHaveAR: false,
    reviews: [],
    __v: 0,
    createdAt: '2022-03-01T06:10:02.561Z',
    updatedAt: '2022-03-01T06:10:02.561Z'
  }
];
