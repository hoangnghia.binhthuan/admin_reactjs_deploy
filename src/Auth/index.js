import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { authLoginAction } from '../redux/actions/authLoginActions';

const Auth = ({ children }) => {
  const dispatch = useDispatch();

  const { loading, success, userInfo } = useSelector((state) => state.authLogin);

  useEffect(() => {
    if (userInfo.token) {
      console.log(
        'Auth Component - useEffect - userInfo.token: ',
        userInfo.token
      );
      // dispatch authLoginAction with token every componentDidMount and componentDidUpdate
      dispatch(authLoginAction({ token: userInfo.token }));
    } else {
      return;
    }
  }, []);

  return <>{ children }</>;
};

export default Auth;
