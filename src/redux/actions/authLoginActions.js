import axios from "axios";
import {
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAIL,
  LOGOUT,
} from "../constants/authLoginConstants";

export const authLoginAction =
  ({ email, password, token }) =>
  async (dispatch) => {
    console.log(
      `authLogin action - email: ${email}, password: ${password}, token: ${token}`
    );

    let userInfoResponse;

    try {
      dispatch({
        type: AUTH_LOGIN_REQUEST,
      });

      if (token) {
        // Check payload is have token => use token to authLogin
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        // todo: create new api in backend (authTokenAdmin) and replace for authToken
        const { data } = await axios.get(
          `${process.env.REACT_APP_BACKEND_HOST}/api/users/authToken`,
          config
        );

        console.log("authLogin action - data", data);
        console.log(
          "authLogin action - env",
          process.env.REACT_APP_BACKEND_HOST
        );

        userInfoResponse = data;
      } else {
        // if not have token => use email and password to authLogin
        const config = {
          header: {
            "Content-Type": "application/json",
          },
        };

        const { data } = await axios.post(
          `${process.env.REACT_APP_BACKEND_HOST}/api/users/login`,
          { email, password },
          config
        );

        console.log("authLogin action - data", data);
        console.log(
          "authLogin action - env",
          process.env.REACT_APP_BACKEND_HOST
        );

        userInfoResponse = data;
      }

      console.log("authLoginActions - userInfoResponse: ", userInfoResponse);

      dispatch({
        type: AUTH_LOGIN_SUCCESS,
        payload: userInfoResponse,
      });

      // After authLogin success, save token to localStorage
      localStorage.setItem("token", userInfoResponse.token);
    } catch (error) {
      // there are 2 kind of error
      // 1. error from client, network error ( -> use error.message)
      // 2. error response from backend server (http error response) (-> use error.response.data)

      // if err dispatch action to save err in redux state
      const errorMessage = error.response
        ? error.response.data.message // err message from server
        : error.message; // err message from client

      dispatch({ type: AUTH_LOGIN_FAIL, payload: errorMessage });
      // remove token from localStorage
      localStorage.removeItem("token");
    }
  };

export const logoutAction = () => (dispatch) => {
  console.log("logout action");

  // remove token from localStorage
  localStorage.removeItem("token");

  dispatch({
    type: LOGOUT,
  });
};
