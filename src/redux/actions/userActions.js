import axios from "axios";
import { toast } from 'react-toastify';
import {
  USER_DELETE_FAIL,
  USER_DELETE_REQUEST,
  USER_DELETE_SUCCESS,
  USER_DETAILS_FAIL,
  USER_DETAILS_REQUEST,
  USER_DETAILS_SUCCESS,
  USER_LIST_FAIL,
  USER_LIST_REQUEST,
  USER_LIST_SUCCESS,
} from "../constants/userConstants";

export const getListUser =
  ({ keyword = "", pageNumber = 1 }) =>
  async (dispatch, getState) => {
    try {
      dispatch({
        type: USER_LIST_REQUEST,
      });

      const { userInfo } = getState().authLogin;

      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
        },
      };

      const { data } = await axios.get(
        `${process.env.REACT_APP_BACKEND_HOST}/api/users?keyword=${keyword}&pageNumber=${pageNumber}`,
        config
      );

      dispatch({ type: USER_LIST_SUCCESS, payload: data });
    } catch (error) {
      dispatch({
        type: USER_LIST_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

  // get user details action
export const getUserDetails = (id) => async (dispatch, getState) => {
  try {
    dispatch({
      type: USER_DETAILS_REQUEST,
    });

    const { userInfo } = getState().authLogin;

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    if (id === 'profile') {
      // call route /api/users/profile -> get user details of user login (ProfileScreen)
      const { data } = await axios.get(
        `${process.env.REACT_APP_BACKEND_HOST}/api/users/profile`,
        config
      );

      dispatch({ type: USER_DETAILS_SUCCESS, payload: data });
    } else {
      // call route /api/users/:id -> admin get user details of any user (UserEditScreen)
      const { data } = await axios.get(
        `${process.env.REACT_APP_BACKEND_HOST}/api/users/${id}`,
        config
      );
      dispatch({ type: USER_DETAILS_SUCCESS, payload: data });
    }
  } catch (error) {
    // if err dispatch action to save err in redux state
    dispatch({
      type: USER_DETAILS_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const deleteUser = (userId) => async (dispatch, getState) => {
  try {
    dispatch({
      type: USER_DELETE_REQUEST,
    });

    const { userInfo } = getState().authLogin;

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    // eslint-disable-next-line
    const { data } = await axios.delete(
      `${process.env.REACT_APP_BACKEND_HOST}/api/users/${userId}`,
      config
    );

    dispatch({ type: USER_DELETE_SUCCESS});
    toast.success('User deleted successfully!');
  } catch (error) {
    dispatch({
      type: USER_DELETE_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};