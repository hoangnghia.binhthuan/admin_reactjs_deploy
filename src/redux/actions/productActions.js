import axios from "axios";
import { toast } from 'react-toastify';
import {
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAIL,
  PRODUCT_LIST_RESET,
  PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAIL,
  PRODUCT_DETAILS_RESET,
  PRODUCT_DELETE_REQUEST,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_DELETE_FAIL
} from "../constants/productContants";

export const listProducts =
  ({ keyword = '', pageNumber = 1 }) =>
  async (dispatch) => {
    console.log('listProducts had dispatch')
    try {
      dispatch({ type: PRODUCT_LIST_REQUEST });

      const { data } = await axios.get(
        `${process.env.REACT_APP_BACKEND_HOST}/api/products?keyword=${keyword}&pageNumber=${pageNumber}`
      );

      dispatch({
        type: PRODUCT_LIST_SUCCESS,
        payload: data,
      });
    } catch (error) {
      // there are 2 kind of error
      // 1. error from client, network err -> (use error.message)
      // 2. error response from backend server (http err response) -> (use error.response.data)

      dispatch({
        type: PRODUCT_LIST_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
  
  export const getProductDetails = (id) => async (dispatch) => {
    try {
      dispatch({ type: PRODUCT_DETAILS_REQUEST });
  
      const { data } = await axios.get(
        `${process.env.REACT_APP_BACKEND_HOST}/api/products/${id}`
      );
  
      dispatch({
        type: PRODUCT_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: PRODUCT_DETAILS_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };

  export const deleteProduct = (productId) => async (dispatch, getState) => {
    try {
      dispatch({ type: PRODUCT_DELETE_REQUEST });
  
      const {
        authLogin: { userInfo },
      } = getState();
  
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
        },
      };
  
      // call api delete product by id to backend server
      // eslint-disable-next-line
      const { data } = await axios.delete(
        `${process.env.REACT_APP_BACKEND_HOST}/api/products/${productId}`,
        config
      );
  
      dispatch({ type: PRODUCT_DELETE_SUCCESS });
      toast.success('Delete product successfully');
    } catch (err) {
      dispatch({
        type: PRODUCT_DELETE_FAIL,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
    }
  };