import {
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAIL, 
  LOGOUT
} from '../constants/authLoginConstants';

export const authLoginReducer = (state = { userInfo: {} }, action) => {
  switch (action.type) {
    case AUTH_LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        success: false,
        error: null
      };

    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: null,
        userInfo: {
          ...state.userInfo,
          ...action.payload
        }
      };

    case AUTH_LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        success: false,
        error: action.payload,
        userInfo: {}
      };

      case LOGOUT:
        return {
          userInfo: {}
        };
  

    default:
      return { ...state };
  }
};
