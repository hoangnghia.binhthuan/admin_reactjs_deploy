import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { authLoginReducer } from "./reducers/authLoginReducers";

import {
  productListReducer,
  productDetailsReducer,
  productDeleteReducer,
} from "./reducers/productReducers";

import { userDetailsReducer, userListReducer, userDeleteReducer } from "./reducers/userReducers";

const rootReducer = combineReducers({
  authLogin: authLoginReducer,
  productList: productListReducer,
  productDetails: productDetailsReducer,
  productDelete: productDeleteReducer,
  userList: userListReducer,
  userDetails: userDetailsReducer,
  userDelete: userDeleteReducer
});

const tokenLocalStorage = localStorage.getItem("token") || "";
console.log("redux/index - tokenLocalStorage: ", tokenLocalStorage);

const initialState = {
  authLogin: {
    userInfo: {
      token: tokenLocalStorage,
    },
  },
};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
