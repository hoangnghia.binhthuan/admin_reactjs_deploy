import { Suspense } from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { useSelector } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import routes from "./router/routes";
import CustomRoute from "./router/CustomRoute";

import Header from "./ui/components/Header";
import Footer from "./ui/components/Footer";
import Loader from "./ui/components/Loader";

function App() {
  const {
    loading: isAuthLoading,
    success: isAuthLoginSuccess,
    error: authLoginError,
    userInfo,
  } = useSelector((state) => state.authLogin);
  return (
    <Router>
      <Header />
      <main className="py-3">
        <Suspense fallback={<Loader />}>
          <ToastContainer position="top-right" autoClose={8000} />
          <Switch>
            {routes.map((route) => {
              return (
                <CustomRoute
                  key={route.path}
                  isAuthLoading={isAuthLoading}
                  isAuthLoginSuccess={isAuthLoginSuccess}
                  token={userInfo.token}
                  {...route}
                />
              );
            })}
          </Switch>
        </Suspense>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
