import { useState, useEffect } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { authLoginAction } from "../../redux/actions/authLoginActions";

import Loader from "../components/Loader";
import Message from "../components/Message";
import FormContainer from "../components/FormContainer";

const LoginScreen = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const searchString = location.search; // the string part of URL, after character '?'
  const searchParams = new URLSearchParams(searchString);

  const redirect = searchParams.has("redirect")
    ? searchParams.get("redirect")
    : "/";

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const {
    loading,
    error,
    success: isAuthLoginSuccess,
    userInfo,
  } = useSelector((state) => state.authLogin);

  useEffect(() => {
    console.log(
      "LoginScreen - useEffect - isAuthLoginSuccess",
      isAuthLoginSuccess
    );
    if (isAuthLoginSuccess) {
      // if authLogin success, redirect to protected page
      history.push(redirect);
      return;
    }
  }, [history, isAuthLoginSuccess]);

  const handleFormSubmit = (e) => {
    e.preventDefault();

    dispatch(authLoginAction({ email, password, token: null }));
  };

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <FormContainer>
          <h1>Sign In</h1>
          {error && <Message variant="danger">{error}</Message>}

          <Form onSubmit={handleFormSubmit}>
            <Form.Group controlId="email">
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Button type="submit" variant="primary" block>
              Continue
            </Button>
          </Form>

          <Row className="mt-3 py-2">
            <Col>
              New Customer ?{" "}
              <Link
                to={redirect ? `/register?redirect=${redirect}` : `/register`}
              >
                Register
              </Link>
            </Col>
          </Row>
        </FormContainer>
      )}
    </>
  );
};

export default LoginScreen;
