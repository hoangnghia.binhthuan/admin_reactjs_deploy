import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Container } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import Loader from "../components/Loader";
import Message from "../components/Message";
import Paginate from "../components/Paginate";
import Meta from "../components/Meta";

import { listProducts } from "../../redux/actions/productActions";

const HomeScreen = () => {
  const params = useParams();
  const keywordUserParam = params.keyword || "";
  const pageNumberParam = params.pageNumber || 1;

  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products, currentPage, totalPages } = productList;

  useEffect(() => {
    dispatch(
      listProducts({ keyword: keywordUserParam, pageNumber: pageNumberParam })
    );
  }, [dispatch, keywordUserParam, pageNumberParam]);

  return (
    <Container>
      <Meta title={`AR Shop | Admin`} />
      <h1>Lasted Product</h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <>
          <Row>
            {products.map((product, index, arr) => {
              return (
                <Col
                  className="my-3"
                  sm={12}
                  md={6}
                  lg={4}
                  xl={3}
                  key={product._id}
                >
                  <ProductCard product={product} />
                </Col>
              );
            })}
          </Row>
          {products.length > 0 && (
            <Paginate
              paginateType="products"
              totalPages={totalPages}
              currentPage={currentPage}
              keyword={keywordUserParam}
            />
          )}
        </>
      )}
    </Container>
  );
};
export default HomeScreen;
