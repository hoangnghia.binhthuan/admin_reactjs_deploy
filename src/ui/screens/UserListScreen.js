import { useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import { Table, Button, Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
import UserSearchBox from "../components/UserSearchBox";
// import { deleteUser, getListUser } from '../../actions/userActions';
import { deleteUser, getListUser } from "../../redux/actions/userActions";
import Meta from "../components/Meta";
import Paginate from "../components/Paginate";

const UserListScreen = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const params = useParams();
  const KeywordAdminParam = params.keyword || "";
  const pageNumberParam = params.pageNumber || 1;

  const userList = useSelector((state) => state.userList);
  const { loading, error, users, totalPages, currentPage } = userList;

  const userDelete = useSelector((state) => state.userDelete);
  const {
    loading: userDeleteLoading,
    error: userDeleteError,
    success: userDeleteSuccess,
  } = userDelete;

  useEffect(() => {
    // user logged in with admin account

    dispatch(
      getListUser({ keyword: KeywordAdminParam, pageNumber: pageNumberParam })
    );
  }, [
    userDeleteSuccess,
    dispatch,
    KeywordAdminParam,
    pageNumberParam,
  ]);

  const userEditHandler = (userId) => {
    history.push(`/admin/user/${userId}/edit`);
  };

  const userDeleteHandler = (userId) => {
    if (window.confirm("Are you sure to delete user?")) {
      dispatch(deleteUser(userId));
    }
  };

  return (
    <Container>
      <Meta title={`Proshop | Admin`} />
      <Row>
        <h1>Users</h1>
      </Row>
      {loading || userDeleteLoading ? (
        <Loader />
      ) : 
      (error  || userDeleteError) ? (
        <Message variant='danger'>{error || userDeleteError}</Message>
      ) : (
        <>
          {users.length === 0 ? (
            <Message>{`There are no user!`}</Message>
          ) : (
            <>
              <Table striped hover responsive bordered className="table-sm">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>ADMIN</th>
                    <th>ACTION</th>
                  </tr>
                </thead>

                <tbody>
                  {users.map((user) => {
                    return (
                      <tr key={user._id}>
                        <td>{user._id}</td>
                        <td>{user.name}</td>
                        <td>
                          <a href={`mailto:${user.email}`}>{user.email}</a>
                        </td>
                        <td>
                          {user.isAdmin ? (
                            <i
                              className="fas fa-check"
                              style={{ color: "green" }}
                            ></i>
                          ) : (
                            <i
                              className="fas fa-times"
                              style={{ color: "red" }}
                            ></i>
                          )}
                        </td>
                        <td>
                          <Button
                            variant="light"
                            className="btn-sm btn-outline-info mr-1"
                            onClick={() => userEditHandler(user._id)}
                          >
                            <i className="fas fa-edit"></i>
                          </Button>

                          <Button
                            variant="danger"
                            className="btn-sm"
                            style={{ borderWidth: "2px" }}
                            onClick={() => userDeleteHandler(user._id)}
                          >
                            <i className="fas fa-trash"></i>
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
              <Paginate
                paginateType="users"
                totalPages={totalPages}
                currentPage={currentPage}
                keyword={KeywordAdminParam}
                isAdmin={true}
              />
            </>
          )}
        </>
      )}
    </Container>
  );
};

export default UserListScreen;
