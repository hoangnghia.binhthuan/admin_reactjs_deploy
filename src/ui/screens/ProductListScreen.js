import { useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
// import { toast } from 'react-toastify';
import { LinkContainer } from "react-router-bootstrap";
import { Table, Button, Row, Col, Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
import Paginate from "../components/Paginate";
import {
  listProducts,
  deleteProduct,
  // createProduct,
} from "../../redux/actions/productActions";
import {
  PRODUCT_DELETE_RESET,
  // PRODUCT_CREATE_RESET,
} from "../../redux/constants/productContants";
import Meta from "../components/Meta";

const ProductListScreen = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const params = useParams();
  const KeywordAdminParam = params.keyword || "";
  const pageNumberParam = params.pageNumber || 1;

  const productList = useSelector((state) => state.productList);
  const { loading, error, products, currentPage, totalPages } = productList;

  const productDelete = useSelector((state) => state.productDelete);
  const {
    loading: productDeleteLoading,
    error: productDeleteError,
    success: productDeleteSuccess,
  } = productDelete;

  // const productCreated = useSelector((state) => state.productCreate);

  // const {
  //   loading: productCreateLoading,
  //   error: productCreateError,
  //   success: productCreateSuccess,
  //   product: productCreate,
  // } = productCreated;

  useEffect(() => {
    if (productDeleteSuccess) {
      dispatch({ type: PRODUCT_DELETE_RESET });
    }

    // if (productCreateSuccess) {
    //   dispatch({ type: PRODUCT_CREATE_RESET });
    //   toast.success('Product created successfully');
    //   history.push(`/admin/product/${productCreate._id}/edit`);
    // }

    dispatch(
      listProducts({ keyword: KeywordAdminParam, pageNumber: pageNumberParam })
    );
    // eslint-disable-next-line
  }, [
    dispatch,
    history,
    productDeleteSuccess,
    // productCreateSuccess,
    KeywordAdminParam,
    pageNumberParam,
  ]);

  const productCreateHandler = () => {
    // dispatch(createProduct());
    return;
  };

  const productDeleteHandler = (productId) => {
    if (window.confirm("Are you sure to delete product?")) {
      dispatch(deleteProduct(productId));
    }
  };

  return (
    <Container>
      <Meta title={`Products | Admin`} />
      <Row className="align-items-center">
        <Col>
          <h1>Products</h1>
        </Col>
        <Col className="text-right">
          <Button className="my-3" onClick={productCreateHandler}>
            <i className="fas fa-plus"></i> Create Product
          </Button>
        </Col>
      </Row>
      {/* {loading || productDeleteLoading || productCreateLoading ? ( */}
      {loading || productDeleteLoading ? (
        <Loader />
      ) : // ) : error || productDeleteError || productCreateError ? (
      error || productDeleteError ? (
        <Message variant="danger">
          {/* {error || productDeleteError || productCreateError} */}
          {error || productDeleteError}
        </Message>
      ) : products.length === 0 ? (
        <Message variant="info">There are no product</Message>
      ) : (
        <>
          <Table striped hover responsive bordered className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>PRICE</th>
                <th>CATEGORY</th>
                <th>BRAND</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {products.map((product) => {
                return (
                  <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>${product.price}</td>
                    <td>{product.category.name}</td>
                    <td>{product.brand}</td>
                    <td className="table-actions">
                      <LinkContainer to={`/product/${product._id}`}>
                        <Button
                          variant="light"
                          className="btn-sm btn-outline-info mr-1 mb-1"
                        >
                          Details
                        </Button>
                      </LinkContainer>

                      <LinkContainer to={`/admin/product/${product._id}/edit`}>
                        <Button
                          variant="light"
                          className="btn-sm btn-outline-info mr-1 mb-1"
                        >
                          <i className="fas fa-edit"></i>
                        </Button>
                      </LinkContainer>

                      <Button
                        variant="danger"
                        className="btn-sm mb-1"
                        style={{ borderWidth: "2px" }}
                        onClick={() => productDeleteHandler(product._id)}
                      >
                        <i className="fas fa-trash"></i>
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          {products.length > 0 && (
            <Paginate
              paginateType="products"
              totalPages={totalPages}
              currentPage={currentPage}
              keyword={KeywordAdminParam}
              isAdmin={true}
            />
          )}
        </>
      )}
    </Container>
  );
};

export default ProductListScreen;
