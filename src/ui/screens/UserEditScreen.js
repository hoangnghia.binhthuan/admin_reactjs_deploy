import { useState, useEffect } from 'react';
import { Link, useHistory, useLocation, useParams } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/Loader';
import Message from '../components/Message';
import { getUserDetails, updateUser } from '../../redux/actions/userActions';
import {
  USER_DETAILS_RESET,
  // USER_UPDATE_RESET,
} from '../../redux/constants/userConstants';
import FormContainer from '../components/FormContainer';

const UserEditScreen = (props) => {
  const history = useHistory();
  const params = useParams();
  const location = useLocation();

  const userId = params.id;

  const dispatch = useDispatch();

  const { userInfo: {token} } = useSelector((state) => state.authLogin);

  const userDetails = useSelector((state) => state.userDetails);
  const { loading, error, user: _userDetails } = userDetails;

  // const userUpdate = useSelector((state) => state.userUpdate);
  // const {
  //   loading: userUpdateLoading,
  //   error: userUpdateError,
  //   success: userUpdateSuccess,
  // } = userUpdate;

  const [name, setName] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
      // userDetails.user === null/underfined || userDetails.user does not match the user we ưant edit
      // the user we ưant edit has the id that match the id param in the url
      if (!_userDetails || !_userDetails.name || userId !== _userDetails._id) {
        dispatch(getUserDetails(userId));
      } else {
        // userDetails is alright
        setName(_userDetails.name);
        setIsAdmin(_userDetails.isAdmin);
      }

    // if (userUpdateSuccess) {
    //   // dispatch({ type: USER_UPDATE_RESET });
    //   dispatch({ type: USER_DETAILS_RESET });
    //   history.push('/admin/userlist');
    // }
  }, [
    dispatch,
    history,
    _userDetails,
    userId,
    // userUpdateSuccess,
  ]);

  const submitHandler = (e) => {
    e.preventDefault();
    return
    // dispatch(updateUser({ _id: userId, name, isAdmin }));
  };

  return (
    <Container>
      <Link to='/admin/userlist' className='btn btn-light my-3'>
        <i className='fas fa-arrow-left'> Go Back</i>
      </Link>
      <FormContainer>
        <h1>Edit User</h1>
        {/* {userUpdateLoading && <Loader />}
        {userUpdateError && (
          <Message variant='danger'>{userUpdateError}</Message>
        )} */}

        {loading ? (
          <Loader />
        ) : error ? (
          <Message variant='danger'>{error}</Message>
        ) : (
          <>
            {_userDetails.socialAccountType === "facebook" && (
              <Form.Group>
                <Message>Sign up with Facebook account !</Message>
              </Form.Group>
            )}
            {_userDetails.socialAccountType === "google" && (
              <Form.Group>
                <Message>Sign up with Google account !</Message>
              </Form.Group>
            )}
            <Form onSubmit={submitHandler}>
              <Form.Group>
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  type='email'
                  value={_userDetails.email}
                  disabled
                  className='cursor-disabled'
                ></Form.Control>
              </Form.Group>

              <Form.Group controlId='name'>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  autoFocus
                  type='text'
                  placeholder='Enter name'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Form.Group controlId='isAdmin'>
                <Form.Check
                  type='checkbox'
                  label='Is Admin'
                  checked={isAdmin}
                  onChange={(e) => setIsAdmin(e.target.checked)}
                ></Form.Check>
              </Form.Group>
              <Button type='submit' variant='primary'>
                Update
              </Button>
            </Form>
          </>
        )}
      </FormContainer>
    </Container>
  );
};

export default UserEditScreen;
