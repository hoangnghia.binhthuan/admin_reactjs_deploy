import { useState, useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../components/Loader";
import Message from "../components/Message";
import { Form } from "react-bootstrap";
import { Row, Col, Image, ListGroup, Container, Button } from "react-bootstrap";
import Rating from "../components/Rating";
import Meta from "../components/Meta";
import { getProductDetails } from "../../redux/actions/productActions";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import ArLabel from "../components/ArLabel";

const ProductScreen = () => {
  const location = useLocation();
  const history = useHistory();
  const { id } = useParams();

  const dispatch = useDispatch();

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, product, error } = productDetails;

  // dispatch getProductDetails Action
  // to fetch single product from server and save in redux state (loading, error, product)
  useEffect(() => {
    dispatch(getProductDetails(id));
  }, [dispatch, id]);

  return (
    <Container>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <>
          <Meta title={`${product.name}`} />
          <div
            className="btn btn-light my-3"
            onClick={() => {
              history.goBack();
            }}
          >
            <i className="fas fa-arrow-left"></i> Go Back
          </div>
          <Row>
            <Col md={6}>
              <div style={{ position: "relative" }}>
                <Image src={product.image} alt={product.name} fluid="true" />
                {product.isHaveAR && <ArLabel />}
              </div>
            </Col>
            <Col md={6}>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <p className="font-weight-bold">{product.name}</p>
                </ListGroup.Item>

                <ListGroup.Item>
                  <Rating
                    value={product.rating}
                    text={`${product.numReviews} reviews`}
                  ></Rating>
                </ListGroup.Item>

                <ListGroup.Item>Price: ${product.price}</ListGroup.Item>

                <ListGroup.Item>
                  Description: {product.description}
                </ListGroup.Item>
              </ListGroup>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <h2>Reviews</h2>
              {product.reviews.length === 0 && (
                <Message>There are no reviews</Message>
              )}
              <ListGroup variant="flush">
                {product.reviews.map((review) => (
                  <ListGroup.Item key={review._id}>
                    <Row>
                      <Col xs={4}>
                        <span className="font-weight-bold">User</span>:
                      </Col>
                      <Col xs={8}>
                        <strong>{review.name}</strong>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={4}>
                        <span className="font-weight-bold">Rating:</span>
                      </Col>
                      <Col xs={8}>
                        <Rating value={review.rating} />
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={4}>
                        <span className="font-weight-bold">Create at:</span>
                      </Col>
                      <Col xs={8}>
                        <p className="m-0">
                          {review.createdAt.substring(0, 10)}
                        </p>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={4}>
                        <span className="font-weight-bold">Review:</span>
                      </Col>
                      <Col xs={12}>
                        <p
                          className="text-body px-1 m-0"
                          style={{ letterSpacing: "normal" }}
                        >
                          {review.comment}
                        </p>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Col>
          </Row>
        </>
      )}
    </Container>
  );
};

export default ProductScreen;
