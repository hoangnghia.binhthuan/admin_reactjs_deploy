import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const UserSearchBox = () => {
  const history = useHistory();

  const [keyword, setKeyword] = useState('');

const submitHandler = (e) => {
    e.preventDefault();
    if (keyword.trim()) {
        history.push(`/admin/userlist/search/${keyword}`)
    } else {
      setKeyword('')
      history.push('/admin/userlist')
    }
  };

  return (
    <Form onSubmit={submitHandler} inline>
      <Form.Control
        name='q'
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
        placeholder='Search User...'
        className='mr-sm-2 ml-sm-5'
      ></Form.Control>
      <Button type='submit' variant='outline-success' className='p1'>
        SEARCH
      </Button>
    </Form>
  );
};

export default UserSearchBox;
