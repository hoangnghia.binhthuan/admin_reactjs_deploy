import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import { Container, Navbar, Nav, Button, NavDropdown } from "react-bootstrap";
import ProductSearchBox from "./ProductSearchBox";
import UserSearchBox from "./UserSearchBox";
import Loader from "./Loader";
import { logoutAction } from "../../redux/actions/authLoginActions";
import { useEffect } from "react";

const Header = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const isShowUserSearchBox = location.pathname.includes('/admin/userlist') ? true : false;

  const {
    loading: isAuthLoginLoading,
    success: isAuthLoginSuccess,
    userInfo,
  } = useSelector((state) => state.authLogin);

  const logoutHandler = () => {
    history.push("/login");
    dispatch(logoutAction());
  };

  return (
    <header>
      <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
        <Container>
          <LinkContainer to="/">
            <Navbar.Brand>AR Shop</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            {isShowUserSearchBox ? <UserSearchBox /> : <ProductSearchBox />}
            {isAuthLoginLoading ? (
              <Nav className="ml-auto">
                <Loader className="ml-auto" />
              </Nav>
            ) : (
              <>
                {isAuthLoginSuccess ? (
                  <>
                    <Nav className="ml-auto">
                      <NavDropdown title={userInfo.name} id="username">
                        <LinkContainer to="/profile">
                          <NavDropdown.Item>Profile</NavDropdown.Item>
                        </LinkContainer>
                        <NavDropdown.Item onClick={logoutHandler}>
                          Logout
                        </NavDropdown.Item>
                      </NavDropdown>
                    </Nav>
                    <Nav>
                      <NavDropdown title="Admin" id="adminmenu">
                        <LinkContainer to="/admin/userlist">
                          <NavDropdown.Item>Users</NavDropdown.Item>
                        </LinkContainer>
                        <LinkContainer to="/admin/categorylist">
                          <NavDropdown.Item>Categories</NavDropdown.Item>
                        </LinkContainer>
                        <LinkContainer to="/admin/productlist">
                          <NavDropdown.Item>Products</NavDropdown.Item>
                        </LinkContainer>
                        <LinkContainer to="/admin/orderlist">
                          <NavDropdown.Item>Orders</NavDropdown.Item>
                        </LinkContainer>
                      </NavDropdown>
                    </Nav>
                  </>
                ) : (
                  <Nav className="ml-auto">
                    <LinkContainer to="/login">
                      <Nav.Link>
                        <i className="fas fa-user"></i> Sign In
                      </Nav.Link>
                    </LinkContainer>
                  </Nav>
                )}
              </>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
