import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useLocation, useHistory } from 'react-router-dom';

const ProductSearchBox = () => {
  const location = useLocation();
  const history = useHistory();

  const isAdminFilter = location.pathname.includes('/admin/productlist');

  const [keyword, setKeyword] = useState('');

const submitHandler = (e) => {
    e.preventDefault();
    if (keyword.trim()) {
      isAdminFilter
        ? history.push(`/admin/productlist/search/${keyword}`)
        : history.push(`/search/${keyword}`);
    } else {
      setKeyword('')
      isAdminFilter ? history.push(`/admin/productlist`) : history.push('/');
    }
  };

  return (
    <Form onSubmit={submitHandler} inline>
      <Form.Control
        name='q'
        onChange={(e) => setKeyword(e.target.value)}
        placeholder='Search Products...'
        className='mr-sm-2 ml-sm-5'
        value={keyword}
      ></Form.Control>
      <Button type='submit' variant='outline-success' className='p2'>
        SEARCH
      </Button>
    </Form>
  );
};

export default ProductSearchBox;
