import React from 'react';

const stylingObject = {
  arLabel: {
    position: 'absolute',
    top: '10px',
    right: '10px',
    cursorPointer: 'none',
    fontSize: '14px',
    color: '#0f5132',
    backgroundColor: '#d1e7dd',
    border: '1px solid transparent',
    borderRadius: '0.25rem',
    borderColor: '#badbcc'
  }
};

const ArLabel = () => {
  return (
    <span className='py-1 px-2 shadow-lg' style={stylingObject.arLabel}>
      AR
    </span>
  );
};

export default ArLabel;
