import React from "react";

const LoginScreen = React.lazy(() => import("../ui/screens/LoginScreen"));
const HomeScreen = React.lazy(() => import("../ui/screens/HomeScreen"));
const ProductScreen = React.lazy(() => import("../ui/screens/ProductScreen"));
const NotFoundScreen = React.lazy(() => import("../ui/screens/NotFoundScreen"));
const ProductListScreen = React.lazy(() =>
  import("../ui/screens/ProductListScreen")
);
const UserListScreen = React.lazy(() => import("../ui/screens/UserListScreen"));
const UserEditScreen = React.lazy(() => import("../ui/screens/UserEditScreen"))

export default [
  {
    title: "Login | AR Shop",
    path: "/login",
    screen: LoginScreen,
    isPrivate: false,
  },
  {
    title: "Product List | Admin",
    path: "/admin/productlist/",
    screen: ProductListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Admin Product",
    path: "/admin/productlist/page/:pageNumber",
    screen: ProductListScreen,
    isPrivate: true,
  },
  {
    title: "Product List | Admin",
    path: "/admin/productlist/search/:keyword",
    screen: ProductListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Product List | Admin",
    path: "/admin/productlist/search/:keyword/page/:pageNumber",
    screen: ProductListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Home | AR Shop",
    path: "/product/:id",
    screen: ProductScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Home | AR Shop",
    path: "/search/:keyword/",
    screen: HomeScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Home | AR Shop",
    path: "/page/:pageNumber",
    screen: HomeScreen,
    isPrivate: true,
  },
  {
    title: "Home | AR Shop",
    path: "/search/:keyword/page/:pageNumber",
    screen: HomeScreen,
    isPrivate: true,
  },
  {
    title: "User Edit | Admin",
    path: "/admin/user/:id/edit",
    screen: UserEditScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "User List | Admin",
    path: "/admin/userlist/",
    screen: UserListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "User List | Admin",
    path: "/admin/userlist/search/:keyword",
    screen: UserListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "User List | Admin",
    path: "/admin/userlist/page/:pageNumber",
    screen: UserListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "User List | Admin",
    path: "/admin/userlist/search/:keyword/page/:pageNumber",
    screen: UserListScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Home | AR Shop",
    path: "/",
    screen: HomeScreen,
    isPrivate: true,
    exact: true,
  },
  {
    title: "Page not found",
    path: "*",
    screen: NotFoundScreen,
    isPrivate: false,
    exact: false,
  },
];
