import { Route, Redirect } from "react-router-dom";
import Loader from "../ui/components/Loader";

function CustomRoute({
  isAuthLoading,
  isAuthLoginSuccess,
  token,
  path,
  exact,
  screen: Screen,
  isPrivate,
}) {
  return (
    <Route
      path={path}
      exact={exact}
      render={() => {
        if (isPrivate) {
          if (isAuthLoading) {
            return <Loader />;
          }

          if (isAuthLoginSuccess) {
            return <Screen />;
          }

          if (!isAuthLoading && !isAuthLoginSuccess && !token) {
            return <Redirect to="/login" />;
          }
        } else {
          return <Screen />;
        }
      }}
    />
  );
}

export default CustomRoute;
