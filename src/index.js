import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './bootstrap.min.css';
import './index.css';
import App from './App';
import reduxStore from './redux';
import Auth from './Auth';

ReactDOM.render(
  <Provider store={reduxStore}>
    <Auth>
      <App />
    </Auth>
  </Provider>,
  document.getElementById('root')
);
